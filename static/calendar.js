const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}


document.addEventListener("DOMContentLoaded", e => {
    for(let i = 2; i < 8; i++)
    {
        let day = new Date();
        day.setDate(day.getDate() + i);

        const dow = days[day.getDay()];
        document.getElementById(i.toString()).getElementsByClassName("header")[0].innerText = dow;
    }

    addShows(httpGet("/shows"));

});

function dateSort(a, b)
{
    a = a.date.split(" ")[1];
    b = b.date.split(" ")[1];

    if(a < b)
        return -1;
    else if (a > b)
        return 1;

    return 0;
}

function addShows(data)
{
    var shows = JSON.parse(data);

    let dayArr = [[],[],[],[],[],[],[],[]];

    for(const i in shows)
    {
        const episode = shows[i];

        if(!episode.finished && episode.days < dayArr.length && episode.days >= 0)
        {
            const show = document.createElement("div");
            show.classList.add("show");

            const img = document.createElement("img");
            img.src = episode.image;
            show.appendChild(img);

            const title = document.createElement("div");
            title.classList.add("title");
            title.innerText = episode.title;
            show.appendChild(title);

            const episodeDiv = document.createElement("div");
            episodeDiv.classList.add("episode");
            episodeDiv.innerText = "Episode " + episode.episode;
            show.appendChild(episodeDiv);

            const countdown = document.createElement("div");
            countdown.classList.add("countdown");
            countdown.innerText = episode.countdown;
            show.appendChild(countdown);

            const date = document.createElement("div");
            date.classList.add("date");
            date.innerText = episode.date;
            show.appendChild(date);

            dayArr[episode.days].push({element: show, date: episode.date});
        }
    }

    for(const i in dayArr)
    {
        let arr = dayArr[i];
        arr.sort(dateSort);

        for(const j in arr)
        {
            document.getElementById(i.toString()).appendChild(arr[j].element);
        }

        if(arr.length == 0)
        {
            const notice = document.createElement("div");
            notice.classList.add("notice")
            if(i == 0)
                notice.innerText = "No more episodes are airing today";
            else
                notice.innerText = "No planned episodes";

            document.getElementById(i.toString()).appendChild(notice);
        }
    }
}


