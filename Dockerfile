from node:16

ENV LANG en_GB.UTF-8
ENV LANGUAGE en_GB:en
ENV LC_ALL nl_BE.UTF-8

workdir /opt/aniwhen

# Copy files
COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 3000
CMD [ "node", "dist/index.js" ]
