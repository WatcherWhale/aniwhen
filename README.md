# AniWhen

Create a week overview of when your favourite anime's air.

## Installation & Startup

Create a json file named `shows.json`. This file contains an array of anilist id's.

example shows.json
```json
[ 1, 1535 ]
```

Install all dependencies and start the server.

```bash
$ npm install

$ npm start
```

## Screenshot

![demo](media/screenshot.png)
