import express from 'express';
import fs from 'fs';
import {LoadShows} from './AniList';

const controller = express.Router();

controller.get("/", async (req, res) => {
    res.send(fs.readFileSync("views/index.html").toString());
});

controller.get("/shows", async (req, res) => {

    const episodes = await LoadShows();
    let arr : any[] = [];

    for (let i = 0; i < episodes.length; i++)
    {
        const episode = episodes[i];
        arr.push(episode.getJson());
    }

    res.contentType("application/json").send(JSON.stringify(arr));

});

export { controller };
