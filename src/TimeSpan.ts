class TimeSpan
{
    private seconds : number;

    constructor(seconds : number)
    {
        this.seconds = seconds;
    }

    getDays() : number
    {
        return Math.floor(this.seconds / (24 * 60 * 60) );
    }

    getHours() : number
    {
        const days = this.getDays() * 24 * 60 * 60;
        return Math.floor( (this.seconds - days) / (60 * 60) );
    }

    getTotalHours() : number
    {
        return Math.floor(this.seconds / (60 * 60) );
    }

    getMinutes() : number
    {
        const hours = this.getTotalHours() * 60 * 60;
        return Math.floor( (this.seconds - hours) / 60 );
    }

    daysFromNow() : number
    {
        const now = new Date();
        const added = new Date(this.seconds + now.getTime());

        const nowDays = Math.floor(now.getTime() / (24 * 60 * 60));
        const addedDays = Math.floor(added.getTime() / (24 * 60 * 60));

        return addedDays - nowDays;
    }

    toString() : string
    {
        let str = "";
        const days = this.getDays();
        const hours = this.getHours();
        const minutes = this.getMinutes();

        if(days > 0)
            str += days + "d "

        if(hours > 0)
            str += hours + "h "

        if(minutes > 0)
            str += minutes + "m"

        if(str[str.length - 1] == " ")
            str = str.substr(0, str.length - 1);

        return str;
    }

    static fromMillis(millis : number) : TimeSpan
    {
        return new TimeSpan(millis / 1000);
    }
}

export { TimeSpan }
