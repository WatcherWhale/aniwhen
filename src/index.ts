import fs from 'fs';
import express from 'express';
import {controller} from './Controller';

const settings = JSON.parse(fs.readFileSync("settings.json").toString());

const app = express();

app.use(express.static("static"));
app.use(controller);

app.listen(settings.port);
