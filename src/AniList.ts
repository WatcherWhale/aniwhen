import fs from 'fs'
import { TimeSpan } from './TimeSpan'

const anilist = require('anilist-node');
const Anilist = new anilist();


class AnimeData
{
    private id : number;
    private title : string;
    private imageUrl : string;

    private finished: boolean;
    private countdown : number = 0;
    private date : Date = new Date(0);
    private nextEpisode : number = 0;


    constructor(data : any, episodeData : any)
    {
        this.id = data.id;
        this.imageUrl = data.coverImage.large;
        this.title = data.title.userPreferred;

        this.finished = data.status == "FINISHED";

        if(!this.finished)
        {
            this.countdown = episodeData.timeUntilAiring;
            this.date = new Date(episodeData.airingAt * 1000);
            this.nextEpisode = episodeData.episode;
        }
    }

    getJson() : any
    {
        if(!this.finished)
        {
            const countdown : TimeSpan = new TimeSpan(this.countdown);
            return {id: this.id, title: this.title, image: this.imageUrl, episode: this.nextEpisode, date: this.date.toLocaleString(), countdown: countdown.toString(), finished: false, days: countdown.daysFromNow()}
        }
        else
        {
            return {id: this.id, title: this.title, image: this.imageUrl, finished: true}
        }
    }

    static async Get(id: number) : Promise<AnimeData[]>
    {
        const data = await Anilist.media.anime(id);
        let animeDataArr : AnimeData[] = [];

        for(const i in data.airingSchedule)
        {
            const episodeData = data.airingSchedule[i];
            if(episodeData.timeUntilAiring > -1 * 24 * 60 * 60 && episodeData.timeUntilAiring < 9 * 24 * 60 * 60)
            {
                animeDataArr.push(new AnimeData(data, episodeData));
            }
        }

        return animeDataArr;
    }
}


async function LoadShows() : Promise<AnimeData[]>
{
    const shows = JSON.parse(fs.readFileSync("shows.json").toString());
    let episodes : AnimeData[] = []

    for (let i = 0; i < shows.length; i++)
    {
        const show = shows[i];

        const episodeArr = await AnimeData.Get(show);
        for(const i in episodeArr)
            episodes.push(episodeArr[i]);
    }

    return episodes;
}


export { LoadShows, AnimeData }

